import { ChangeEvent, useState, useContext } from 'react';
import {v4 as uuidv4} from 'uuid';

import { Box, Button, TextField } from '@mui/material'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Cancel';

import { Entry } from '../../interfaces';
import { EntriesContext } from '../../context/entries/EntriesContext';
import { UIContext } from '../../context/ui';

export const NewEntry = () => {

    // Context 
    const { addNewEntry } = useContext(EntriesContext);
    const { isAddNewEntry, setIsAddNewEntry } = useContext(UIContext);

    // agregando
    // const [isAddNewEntry, setisAddNewEntry] = useState(false);

    // Cabiando texto
    const [inputText, setInputText] = useState('');

    // Touch
    const [touch, setTouch] = useState(false);

    const handleInputChange = ( event: ChangeEvent<HTMLInputElement> ) => {
        setInputText( event.target.value);
    }

    const handleSumit = ( ) => {

        // Validacion
        if( inputText.length <= 0 ) return;

        // disopacth
        addNewEntry( inputText );

        // limpia input
        setInputText(''); 
        setIsAddNewEntry(false);
        setTouch(false);
        
    }

    const handelCancel = () => {
        setInputText(''); 
        setIsAddNewEntry(false);
        setTouch(false);
    }


  return (
    <Box
        sx={{
            padding: 1,
            marginBottom: 2,
        }}
    >

        {
            isAddNewEntry ?
            (
                <>
                      <TextField 
                            fullWidth
                            multiline
                            autoFocus
                            placeholder='New Entry'
                            label='New Entry'
                            sx={{
                                arginTop: 2,
                                marginBottom: 1,
                            }}
                            onChange={ handleInputChange }
                            value={ inputText }
                            onBlur={ () => setTouch( true ) } // cuando pierde el auotFocus
                            helperText={ inputText.length <= 0 && touch && `Insert text` }
                            error={ inputText.length <= 0 && touch }
                            />

                    <Box
                        display={ 'flex' }
                        justifyContent='space-between'
                    >
                        <Button
                            startIcon={ <CancelIcon />}
                            onClick={ handelCancel}
                            
                        >
                            Cancel
                        </Button>

                        <Button
                            variant='outlined'
                            color='secondary'
                            endIcon={ <SaveIcon /> }
                            onClick={ handleSumit }
                        >
                            Saved
                        </Button>

                    </Box>
                </>
            ) :
            (
                <Button
                    fullWidth // ancho max de la caja
                    variant='outlined'
                    startIcon={ <AddCircleOutlineIcon />}

                    onClick={ () => setIsAddNewEntry( true ) }
                >
                    Add Task
                </Button>
            )
        }

    </Box>
  )
}
