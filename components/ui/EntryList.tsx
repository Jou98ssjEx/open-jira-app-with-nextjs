import React, { DragEvent, FC, useMemo, useContext } from 'react'

import { List, Paper } from '@mui/material'

import { EntryStatus } from '../../interfaces'
import { EntryCard } from './'
import { EntriesContext } from '../../context/entries';
import { UIContext } from '../../context/ui';

import style from './EntryList.module.css'

interface Props {
    status: EntryStatus;
}

export const EntryList: FC <Props> = ( { status } ) => {

    const { entries, updatedEntry } = useContext(EntriesContext);

    const { isDragging, setIsDragEnd } = useContext( UIContext );

    // filtrar los stados y memorizar para que se renderizen SOLO cuando cambien
    const entryByStatus = useMemo(() => 
        entries.filter( data => data.status === status ), [ entries ]);

    const allwDrop = ( event: DragEvent<HTMLDivElement> ) => {
        event.preventDefault();
    }

    const onDropEntry = ( event: DragEvent<HTMLDivElement> ) =>{
        const id = event.dataTransfer.getData('text');
        // console.log({id})

        const entry = entries.find( entry => entry._id === id )!; // ! confia en mi, siempre vas a tener un valor
        entry.status = status;
        updatedEntry(entry); 
        setIsDragEnd();
    }


  return (
    //   TODO: Aqui haremos drop
    <div
        onDrop={ onDropEntry }
        onDragOver={ allwDrop }
        className={ isDragging ? style.dragging : ''}
    >
        <Paper
            sx={{
                height: 'calc(100vh - 250px)',
                overflowY: 'auto',
                backgroundColor: 'transparent',
                padding: 1,
                '&::-webkit-scrollbar': { display: 'none' }, // para sacar el scrool
            }}
        >
            <List
                sx={{
                    opacity: isDragging ? 0.5 : 1,
                    transition: 'all .3s'
                }}
            >
                {/* Componente de card */}
                {
                    entryByStatus.map( data => (
                        <EntryCard 
                            key={data._id}
                            entry={ data }
                        />

                    ))
                }
            </List>
        </Paper>
    </div>
  )
}
