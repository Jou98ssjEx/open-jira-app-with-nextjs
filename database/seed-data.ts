import { EntryStatus } from "../interfaces";

interface SeedData {
    entries: SeedEntry[];
}

interface SeedEntry {
    description: string,
    createdAt: number,
    // status: string,
    status: EntryStatus,
}

export const seedData: SeedData = {
    entries: [
        {
            description: 'Pending..',
            createdAt: Date.now(),
            status: 'pending',
        },
        {
            description: 'In-Progress..',
            createdAt: Date.now(),
            status: 'in-progress',
        },
        {
            description: 'Finished..',
            createdAt: Date.now(),
            status: 'finished',
        },
    ]
}