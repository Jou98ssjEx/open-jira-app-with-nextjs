import { UIState } from './';

type UIActionType = 
    | { type: '[UI] - Open Sidebar'}
    | { type: '[UI] - Close Sidebar'}
    | { type: '[UI] - Open-Close Entry', payload: boolean}
    | { type: '[UI] - Drag True'}
    | { type: '[UI] - Drag False'}
    // | { type: '[UI] - Close Entry', payload: boolean}

export const uiReducer  = ( state: UIState, action: UIActionType ): UIState => {
    switch (action.type) {

        case '[UI] - Open Sidebar':
            return {
                ...state,
                sideMenuOpen: true,
            }         

        case '[UI] - Close Sidebar':
            return {
                ...state,
                sideMenuOpen: false,
            } 
            
        case '[UI] - Open-Close Entry':
            return{
                ...state,
                isAddNewEntry: action.payload,
            }
        
        case '[UI] - Drag True':
            return{
                ...state,
                isDragging: true,
            }
        case '[UI] - Drag False':
            return{
                ...state,
                isDragging: false,
            }

        default:
            return state;
    }
} 
