import { FC, useReducer } from "react";
import { UIContext, uiReducer } from "./";
import React from 'react';


export interface UIState {
    sideMenuOpen: boolean;
    isAddNewEntry: boolean;
    isDragging: boolean;
}

const UI_INITIAL_STATE: UIState = {
    sideMenuOpen: false,
    isAddNewEntry: false,
    isDragging: false,
    
}

interface Props {
    children?: React.ReactNode
}

export const UIProvider: FC <Props> = ( {children} ) => {

    const [state, dispatch] = useReducer(uiReducer, UI_INITIAL_STATE );

    const openSideMenu = ( ) => {
        dispatch( { type: '[UI] - Open Sidebar' });
    }

    const closeSideMenu = ( ) => {
        dispatch( { type: '[UI] - Close Sidebar'});
    }

    const setIsAddNewEntry = ( status: boolean ) => {
        dispatch({type: "[UI] - Open-Close Entry", payload: status})
    }

    const setIsDragStart = () =>{
        dispatch({
            type:'[UI] - Drag True',
        })
    }

    const setIsDragEnd = () => {
        dispatch({
            type:'[UI] - Drag False',
        })
    }

    return(
        <UIContext.Provider value={{
            ...state,
            openSideMenu,
            closeSideMenu,
            setIsAddNewEntry,
            setIsDragStart,
            setIsDragEnd,
         }}
        >
            { children }
        </UIContext.Provider>
    )

}