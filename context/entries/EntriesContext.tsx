import { createContext } from 'react';
import { Entry } from '../../interfaces';

interface ContextProps {
    entries: Entry[];
    addNewEntry: ( descripcion: string ) => void;
    updatedEntry: ( data: Entry, snackbar?: boolean ) => void;
    deleteEntry: ( _id: string ) => void;
}

 export const EntriesContext = createContext({} as ContextProps);