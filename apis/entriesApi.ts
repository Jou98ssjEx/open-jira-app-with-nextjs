import axios from "axios";


const entryApi = axios.create({
    baseURL:`/api`
})

export default entryApi;