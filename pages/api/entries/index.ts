import type { NextApiRequest, NextApiResponse } from 'next'
import { db } from '../../../database'
import { EntryModel, IEntry } from '../../../models'

type Data = 
    {
        ok: boolean
        msg: string
    }
  |
    {
        ok: boolean,
        msg: string,
        entries: IEntry [],
    }
  |
  {
      ok: boolean,
      msg: string,
      entry: IEntry
  }


export default function (req: NextApiRequest, res: NextApiResponse<Data>) {
    // res.status(200).json({ msg: 'Example' })

    switch (req.method) {
        case 'GET':
            return getEntries( res );

        case 'POST':
            return postEntry( req, res );
        
       
        default:
            return res.status(400).json({
                ok: false,
                msg:`No Authorizado`
            })
    }
}

const getEntries = async ( res:NextApiResponse<Data> ) => {

    // open connection db
    await db.connect();

    // Cuerpo como controlador
    const entries = await EntryModel.find();

    // disconnect db
    await db.disconnect();

    // respuesta
    res.status(200).json({
        ok: true,
        msg:`Entires`,
        entries,
    })

}

const postEntry = async ( req:NextApiRequest , res: NextApiResponse<Data> ) => {
    
    // sacar la desc del req
    const { description } = req.body;
    // console.log(description);

    const entry = new EntryModel({
        description,
        createdAt: Date.now(),
    });

    
    try {

        db.connect();
        entry.save();
        db.disconnect();

        res.status(201).json({
            ok: true,
            msg:`Add Entry`,
            entry
        })

        

    } catch (error) {
        
        res.status(500).json({
            ok: false,
            msg:`Fallo algo`
        })
    }

}

