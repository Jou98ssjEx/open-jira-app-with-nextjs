import { NextFetchEvent, NextRequest, NextResponse } from "next/server";
import mongoose from 'mongoose';


export function middleware (req: NextRequest, event: NextFetchEvent ){

/**
 * Para no crear carpetas para aplicar el middleware: recomendado crear carpeta
 */
    // if (req.page.name === '/api/entries') return NextResponse.next(); 

    const id = req.page.params?.id || '';

    const checkMongoIDRegExp = new RegExp("^[0-9a-fA-F]{24}$");

    if (!checkMongoIDRegExp.test( id ) ) {

        return new Response( JSON.stringify({
            msg: `Id no valido: ${ id }`
        }),{
            status: 400,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }

    return NextResponse.next();
}