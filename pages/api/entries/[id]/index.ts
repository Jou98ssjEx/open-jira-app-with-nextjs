import type { NextApiRequest, NextApiResponse } from 'next'
import { EntryModel, IEntry } from '../../../../models'
import mongoose from 'mongoose';
import { db } from '../../../../database';

type Data = 
{
    ok: boolean
    msg: string
}
|
{
    ok: boolean,
    msg: string,
    entries: IEntry [],
}
|
{
  ok: boolean,
  msg: string,
  entry: IEntry
}

export default function (req: NextApiRequest, res: NextApiResponse<Data>) {
    // res.status(200).json({ name: 'Example' })

    switch (req.method) {
        case 'GET':
            return getEntryById( req, res );

        case 'PUT':
            return updateEntry( req, res );

        case 'DELETE':
            return deleteEntry( req, res );
    
        default:
            return res.status(400).json({
                ok: false,
                msg:`No Authorizado`
            })
    }

}

const isMongoID = ( id: string | string[],  res: NextApiResponse<Data> ) => {
    if ( !mongoose.isValidObjectId( id ) ){
        return res.status(400).json({
            ok: false,
            msg: `Id no valido ${ id } `
        })
    }
}

const getEntryById = async ( req:NextApiRequest , res: NextApiResponse<Data> ) => {

    const { id } = req.query;

    // validar id de mongo
    isMongoID( id , res);

    try {

        const entryFound = await EntryModel.findById( id );
        
        res.status(200).json({
            ok: true,
            msg: `Data found`,
            entry: entryFound!
        })
    } catch (error) {
        db.disconnect();
        
        res.status(200).json({
            ok: true,
            msg: `Error`,
        })
    }

    
    
}



const updateEntry = async ( req:NextApiRequest , res: NextApiResponse<Data> ) => {

    const { id } = req.query; // aqui no hay params: axios

     // validar id de mongo
     isMongoID( id , res);

    try {

        await db.connect();

        const entryFound = await EntryModel.findById(id);

        if ( !entryFound ){

            await db.disconnect();
            
            return res.status(401).json({
                ok: false,
                msg: `No se encontro registro`,
            })
        }

        // si se envia data
        const { 
            description = entryFound.description,
            status = entryFound.status,
        } = req.body;

        // actualiza

        const updatedEntry = await EntryModel.findByIdAndUpdate( id, {
            description,
            status,
        }, {
            new: true,
            runValidators: true,
        })

        
        res.status(200).json({
            ok: true,
            msg: 'Put',
            entry: updatedEntry!, // !: indica que si trae data
        })

    } catch (error) {
        db.disconnect();
        res.status(400).json({
            ok: false,
            msg: 'Mala respuesta'
        })
    }
}
    

const deleteEntry = async ( req:NextApiRequest , res: NextApiResponse<Data> ) => {
    
    const { id } = req.query; // aqui no hay params: axios

     // validar id de mongo
     isMongoID( id , res);

    try {

        await db.connect();

     

        const deletedEntry = await EntryModel.findByIdAndDelete( id, {
            new: true,
            runValidators: true,
        })

        
        res.status(200).json({
            ok: true,
            msg: 'delete',
            entry: deletedEntry!, // !: indica que si trae data
        })

    } catch (error) {
        db.disconnect();
        res.status(400).json({
            ok: false,
            msg: 'Mala respuesta'
        })
    }
}