import { Card, CardContent, CardHeader, Grid, Typography } from '@mui/material'
import type { NextPage } from 'next'
import { Layout } from '../components/layouts'
import { EntryList, NewEntry } from '../components/ui'
import { Entry } from '../interfaces'
import { useContext } from 'react';
import { EntriesContext } from '../context/entries/EntriesContext';


const HomePage: NextPage  = ( ) => {


  return (
  <Layout title='Home'>
    <Grid container spacing={ 2 }>

      <Grid item xs={12} sm={4}>
          <Card sx={ { height: 'calc(100vh - 100px)'} }>
            <CardHeader  title={ `Pendientes:` } />
              <NewEntry />
              <EntryList  status='pending'/>
        </Card>
      </Grid>

      <Grid item xs={12} sm={4}>
        <Card sx={ { height: 'calc(100vh - 100px)'} }>
          <CardHeader  title={ `In-Progress:` } />
            <EntryList  status={ `in-progress` }/>
        </Card>
      </Grid>

      <Grid item xs={12} sm={4}>
        <Card sx={ { height: 'calc(100vh - 100px)'} }>
          <CardHeader  title={ `Finished:` } />
            <EntryList  status={ `finished` }/>
        </Card>
      </Grid>

      
    </Grid>
  </Layout>
  )
}

export default HomePage
