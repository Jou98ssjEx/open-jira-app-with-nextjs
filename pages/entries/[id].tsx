import React, { ChangeEvent, FC, useMemo, useState, useContext } from 'react';
import { GetServerSideProps } from 'next'

import { Grid, TextField, Box, Card, CardHeader, CardContent, CardActions, Button, FormControl, RadioGroup, FormControlLabel, Radio, FormLabel, IconButton, capitalize } from '@mui/material';
import SaveOutlinedIcon from '@mui/icons-material/SaveOutlined';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';

import { Layout } from '../../components/layouts';
import { Entry, EntryStatus } from '../../interfaces';
import { dbEntry } from '../../database';
import { EntriesContext } from '../../context/entries/EntriesContext';
import { useRouter } from 'next/router';
import { dateFuntions } from '../../utils';

// status de las entradas
const validStatus: EntryStatus[] = ['pending', 'in-progress', 'finished'];

interface Props {
    entry: Entry
}

export const EntryPage: FC<Props> = ( { entry } ) => {


    const { updatedEntry, deleteEntry } = useContext(EntriesContext);

    const router = useRouter();

    const [inputValue, setInputValue] = useState(entry.description);
    const [status, setStatus] = useState <EntryStatus>(entry.status);
    const [touch, setTouch] = useState(false);

    // Memorizar un valor para mejorar el rendimiento
    const isNotValid = useMemo(() => inputValue.length <= 0 && touch, [inputValue, touch])

    const handleInputChange = ( event: ChangeEvent<HTMLInputElement> ) => {
        setInputValue( event.target.value );
    }

    const onStatudChanged = ( event: ChangeEvent<HTMLInputElement> ) => {
        // console.log(event.target.value)
        setStatus( event.target.value as EntryStatus );
    }

    const handleUpdate = ( ) => {
        console.log({
            id: entry._id,
            description: inputValue,
            createAt: entry.createdAt,
            status,
        });

        // update
        updatedEntry({
            _id: entry._id,
            description: inputValue,
            createdAt: entry.createdAt,
            status,
        }, true);

        router.push( `/`)
    }

    const handleDelete = ( ) => {
        console.log(entry._id);

        deleteEntry( entry._id)

        router.push( `/`)

    }

  return (
    <Layout
        title={`Update note`}
    >

        <Grid
            container
            justifyContent={`center`}
            sx={{
                padding: 1,
                marginTop: 3,
            }}
        >
            <Grid 
                item
                xs={ 12 } sm={ 8 } md={ 6 }
            >
                <Card>

                    <CardHeader 
                        title={`Entry: ${capitalize( entry.status)} `}
                        subheader={` ${dateFuntions.getFormatDistanceToNow( entry.createdAt ) } `}
                    />


                    <CardContent>
                        <TextField 
                            fullWidth
                            autoFocus
                            multiline
                            placeholder='New Entry'
                            label={`New Entry`}
                            value={ inputValue}
                            onChange={ handleInputChange }
                            onBlur={ () => setTouch( true ) } // cuando pierde el auotFocus
                            helperText={ isNotValid && `Insert text` }
                            error={ isNotValid }
                            sx={{
                                // mt: 2,
                                mb: 2
                            }}
                        
                        />

                        <FormControl>
                            <FormLabel>Status: </FormLabel>
                            <RadioGroup 
                                row
                                // key={ status }
                                value={ status }
                                onChange={ onStatudChanged }
                            >
                                {
                                    validStatus.map( opt => (
                                        <FormControlLabel 
                                            key={ opt }
                                            value={ opt }
                                            control={ <Radio /> } // que elemento va usar
                                            label={ capitalize(opt) }
                                        />
                                    ))
                                }
                            </RadioGroup>
                        </FormControl>
                    </CardContent>

                    <CardActions>
                        <Button
                            fullWidth
                            startIcon={ <SaveOutlinedIcon /> }
                            variant='contained'
                            disabled={ inputValue.length <= 0 }
                            onClick={ handleUpdate }
                        >
                            Save
                        </Button>
                    </CardActions>
                </Card>

                <IconButton
                    sx={{
                        position: 'fixed',
                        bottom: 30,
                        right: 30,
                        backgroundColor: 'error.dark'
                    }}
                    onClick={ handleDelete }
                >
                    <DeleteOutlineOutlinedIcon />
                </IconButton>

            </Grid>
        </Grid>


    </Layout>
  )
}


// You should use getServerSideProps when:
// - Only if you need to pre-render a page whose data must be fetched at request time

// SSR
export const getServerSideProps: GetServerSideProps = async ({params}) => {
    // const { data } = await  // your fetch function here 

    const { id } = params as { id: string};

    const entry = await dbEntry.EntryById(id);

    if (!entry ) {
        return{
            redirect:{
                destination:'/',
                permanent: false
            }
        }
    }

    return {
        props: {
            entry: JSON.parse( JSON.stringify( entry )), // para la serializacion: quitar el Object ID
        }
    }
}


export default EntryPage;